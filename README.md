# LES CARNETS DE L'ISTHMOGRAPHE

Vous pouvez aussi retrouver [ces carnets](https://poetisthme.cargo.site/L-ISTHMOGRAPHE) en vous aventurant dans notre thébaïde des internets.

## L'ISTHMOGRAPHE ou l'Observatoire des Poétiques du Lien

L’ISTHMOGRAPHE se consacre à l’extension du domaine de l’isthme poétique au travers de la découverte d’artistes dont la poétique tend à dépasser le clivage objectif – subjectif.

L’ISTHMOGRAPHE est à l’origine d’une tentative alchimique de superposer l’objectif et le subjectif, autrement dit d’opérer une fusion de l’émotion en matière, de la matière en émotion…

L’ISTHMOGRAPHE est publié dans des « carnets » distincts des numéros de la revue dans la mesure où ils possèdent une

individualité propre quand bien même il reste in fine en lien avec le développement du poétisthme.

L’ISTHMOGRAPHE accorde une place égale à la création littéraire et audiovisuelle – l’expérimentation plastique étant partie intégrante de l’expérience poétique.

L’ISTHMOGRAPHE c’est un foyer de la création – il la manifeste mais prétend pas avoir une valeur de manifeste. Chaque carnet concentre et déploie la vision d’un artiste.

## Notre vision du partage

Ces [carnets](https://poetisthme.cargo.site/Partage) sont disponibles sous licence Creative Commons Attribution – Pas d’Utilisation Commerciale – Partage dans les Mêmes Conditions 4.0 International (CC-BY-NC-SA 4.0).

## Le Collectif

POÉTISTHME est un collectif dédié à l’expérimentation poétique via l’édition, l’organisation d’évènements et d’interventions en milieu scolaire. Nous avons à coeur de promouvoir la poésie contemporaine et la faire vivre là où elle est nécessaire (c’est-à-dire partout) mais n’est pas nécessairement présente. Si vous désirez en savoir plus à notre sujet et nous rejoindre sur le chantier du rêve, nous vous invitons à consulter notre [Foyer des internets](https://poetisthme.cargo.site/). 

## Nous soutenir

Pour adhérer à notre association ou nous faire don d'une obole pour soutenir nos projets, c'est [ici](https://www.helloasso.com/associations/poetisthme)
